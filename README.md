## Solution


### Approach

Having identified both the class of problem and the available methods for solving it, it was determined that the best method to employ was the Hungarian algorithm, due to it's efficiency and availability of an existing package ([munkres-js](https://github.com/addaleax/munkres-js)) that implements it in the chosen language. Since the role being considered for requires proficiency in `node.js`, the selection of `node` as the implementation language was a natural choice.

The solution can be broken down into the following steps:

1. Read the input data files that contain the names of drivers and the addresses of destinations that need to be assigned
2. Take the inputs and construct a reward matrix based on the suitability score algorithm, as well as construct a matrix that pairs all drivers to all tasks where each entry corresponds to an associated entry in the rewards matrix. This correspondence of rewards to driver-to-destination pairings will be used to correspond final assignments based on the rewards matrix to actual driver-to-destination pairings that can be displayed to the user in a human-readable format.
3. Convert the reward matrix into a cost matrix
4. Feed the cost matrix as input to the [munkres-js](https://github.com/addaleax/munkres-js) package to generate the matrix of least-cost assignments
5. Use the least-cost assignments and the reward matrix to produce the total suitability score
6. Map the least-cost assignment matrix to driver/destination pairs associated with the assignments
7. Display the results

### Deliverables

- The full source code, including any code written which is not part of the normal program run (e.g. build scripts)
- Clear instructions on how to build/run the app

### Installation

1. Install `node.js` (version >= 9.11.2), if not already installed:
   - See [instructions](https://nodejs.org/en/download/package-manager/) or google.
2. Install `git`, if not already installed:
   - See [instructions](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) or google.
3. Download code:
   - Run/type `git clone https://gitlab.com/armenterosroilan/drisvers_directions.git` in terminal 
4. Enter `drisvers_directions` project directory created by above step:
   - Run/type `cd drisvers_directions`
5. Install package dependencies:
   - Run/type `npm install`

### Usage

1. Use two text data files, one with the list of driver names, and the other with the list of destination addresses. Each line of the drivers file should have a single driver name. Similarly, each line of the destinations file should have a single destination address. Recall that this will be a balanced assignment, so there should be the same number of drivers as there are destinations.

2. Start the application by running/typing '`npm run start`' from within the project directory (i.e. '`drisvers_directions`').

3. The application will prompt the user to enter the filepaths to the data files created in step 1 above:

   1.  `? Enter the path to the destinations list file` :
      - Type/enter the path (e.g. `test/directions.txt`)

   2.  `? Enter the path to the drivers list file` :
      - Type/enter the path (e.g. `test/drivers.txt`)

### Output

- The application will display on the console/terminal the total suitability score, along with the set of optimal driver/destination assignments.
- Example run:

![Example output](img/output.png?raw=true)

